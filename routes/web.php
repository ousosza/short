<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('','TodoController');


//Route::get('/new',function (){
//   return view('new');
//});

Route::get('/','TodoController@index');
Route::get('/t/{short}','TodoController@show');
Route::get('/data','TodoController@data');
