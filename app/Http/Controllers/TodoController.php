<?php

namespace App\Http\Controllers;
use App\short172DB;
use Illuminate\Http\Request;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shorts = short172DB::OrderBy('id','asc')->get();
        return view('index')->with('shorts',$shorts);

    }

    public function data()
    {
        return view('data');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
                'moviename'=>'required',
                'Description'=>'required',
                'images'=>'required|mimes:jpg,jpeg,png,gif|max:2048',
            ]
        );



//        dd($request->file());
        $file_name = time().'.'.$request->images->extension();
        $request->images->move(public_path('uploads'),$file_name);


        $todo = new Todo();
        $todo->moviename = $request->input('moviename');
        $todo->Description = $request->input('Description');
        $todo->file_name= $file_name;
        $todo->save();



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($shorturl)
    {


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
